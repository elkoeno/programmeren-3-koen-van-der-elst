﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Webwinkel.Controllers
{
    public class CountryController : Controller
    {
        public ActionResult ReadingAll()
        {
            Models.Dal dal = new Models.Dal();
            return View(dal.DbSetCountry);
        }

        public ActionResult Editing()
        {
            return View();
        }

        public ActionResult Inserting()
        {
            //Models.Dal dal = new Models.Dal();
            return View(/*dal.DbSetCountry*/);
        }

        [HttpPost]
        public ActionResult Insert(string CountryCode, string CountryName,
        float CountryLatitude, float Countrylongitude)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country();
            country.Code = CountryCode;
            country.Name = CountryName;
            country.Latitude = CountryLatitude;
            country.longitude = Countrylongitude;
            if (TryValidateModel(country))
            {
                dal.DbSetCountry.Add(country);
                dal.SaveChanges();
            }
            return View("Inserting", dal.DbSetCountry);
        }

        public ActionResult InsertingCancel()
        {
            Models.Dal dal = new Models.Dal();
            return View("Editing", dal.DbSetCountry);
        }

        public ActionResult ReadingOne(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country();
            country.Id = id;
            country = dal.DbSetCountry.Find(id);
            return View(country);
        }

        public ActionResult Cancel()
        {
            return View("Editing");
        }

        public ActionResult Updating(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country();
            country.Id = id;
            country = dal.DbSetCountry.Find(id);
            return View(country);
        }

        [HttpPost]
        public ActionResult Update(string CountryCode, string CountryName,
        float CountryLatitude, float Countrylongitude, string CountryId)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country();
            country.Id = Int32.Parse(CountryId);
            country.Code = CountryCode;
            country.Name = CountryName;
            country.Latitude = CountryLatitude;
            country.longitude = Countrylongitude;
            if (TryValidateModel(country))
            {
                dal.DbSetCountry.Attach(country);
                dal.Entry(country).State = EntityState.Modified;
                dal.SaveChanges();
            }
            return View("ReadingOne", country);
        }

        public ActionResult Delete(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.Country country = new Models.Country() { Id = id };
            dal.DbSetCountry.Attach(country);
            dal.DbSetCountry.Remove(country);
            dal.SaveChanges();
            return View("Editing");
        }
    }
}