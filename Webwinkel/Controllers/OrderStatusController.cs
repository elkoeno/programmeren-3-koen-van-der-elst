﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Webwinkel.Controllers
{
    public class OrderStatusController : Controller
    {
        public ActionResult Inserting()
        {
            //Models.Dal dal = new Models.Dal();
            return View(/*dal.DbSetUnitBase*/);
        }
        public ActionResult Editing()
        {
            //Models.Dal dal = new Models.Dal();
            return View(/*dal.DbSetUnitBase*/);
        }

        [HttpPost]
        public ActionResult Insert(string OrderStatusCode, string OrderStatusName,
        string OrderStatusDescription)
        {
            Models.Dal dal = new Models.Dal();
            Models.OrderStatus orderStatus = new Models.OrderStatus();
            orderStatus.Name = OrderStatusName;
            orderStatus.Description = OrderStatusDescription;
            if (TryValidateModel(orderStatus))
            {
                dal.DbSetOrderStatus.Add(orderStatus);
                dal.SaveChanges();
            }
            return View("Inserting", dal.DbSetOrderStatus);
        }

        public ActionResult InsertingCancel()
        {
            Models.Dal dal = new Models.Dal();
            return View("Editing", dal.DbSetOrderStatus);
        }

        public ActionResult ReadingOne(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.OrderStatus orderStatus = new Models.OrderStatus();
            orderStatus.Id = id;
            orderStatus = dal.DbSetOrderStatus.Find(id);
            return View(orderStatus);
        }

        public ActionResult Cancel()
        {
            return View("Editing");
        }

        public ActionResult Updating(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.OrderStatus orderStatus = new Models.OrderStatus();
            orderStatus.Id = id;
            orderStatus = dal.DbSetOrderStatus.Find(id);
            return View(orderStatus);
        }

        [HttpPost]
        public ActionResult Update(string OrderStatusName,
        string OrderStatusDescription, string OrderStatusId)
        {
            Models.Dal dal = new Models.Dal();
            Models.OrderStatus orderStatus = new Models.OrderStatus();
            orderStatus.Id = Int32.Parse(OrderStatusId);
            orderStatus.Name = OrderStatusName;
            orderStatus.Description = OrderStatusDescription;
            if (TryValidateModel(orderStatus))
            {
                dal.DbSetOrderStatus.Attach(orderStatus);
                dal.Entry(orderStatus).State = EntityState.Modified;
                dal.SaveChanges();
            }
            return View("ReadingOne", orderStatus);
        }

        public ActionResult Delete(int id)
        {
            Models.Dal dal = new Models.Dal();
            Models.OrderStatus orderStatus = new Models.OrderStatus() { Id = id };
            dal.DbSetOrderStatus.Attach(orderStatus);
            dal.DbSetOrderStatus.Remove(orderStatus);
            dal.SaveChanges();
            return View("Editing");
        }

        public ActionResult ReadingAll()
        {
            Models.Dal dal = new Models.Dal();
            return PartialView(dal.DbSetOrderStatus);
        }
    }
}