﻿using System.Data.Entity;

namespace Webwinkel.Models
{
    public class Dal : DbContext
    {
        public Dal()
            : base("name=WebwinkelWindowsAuthentication")
        {
        }

        public virtual DbSet<UnitBase> DbSetUnitBase { get; set; }
        public virtual DbSet<Country> DbSetCountry { get; set; }
        public virtual DbSet<Customer> DbSetCustomer { get; set; }
        public virtual DbSet<OrderStatus> DbSetOrderStatus { get; set; }
        public virtual DbSet<Category> DbSetCategory { get; set; }
        public virtual DbSet<Supplier> DbSetSupplier { get; set; }

        // http://stackoverflow.com/questions/2614941/unique-keys-in-entity-framework-4
        public class Initializer : IDatabaseInitializer<Dal>
        {
            public void InitializeDatabase(Dal context)
            {
                if (!context.Database.Exists() || !context.Database.CompatibleWithModel(false))
                {
                    if (context.Database.Exists())
                    {
                        context.Database.Delete();
                    }
                    context.Database.Create();
                }
            }
        }

        public System.Data.Entity.DbSet<Webwinkel.Models.Product> Products { get; set; }
    }
}