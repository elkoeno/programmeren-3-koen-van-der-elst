﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Webwinkel.Models
{
    public class OrderStatus
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is verplicht.")]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255, ErrorMessage = "{0} kan maximum 255 karakters bevatten.")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(1024, ErrorMessage = "{0} kan maximum 1024 karakters bevatten.")]
        [DisplayName("Beschrijving")]
        public string Description { get; set; }

    }
}