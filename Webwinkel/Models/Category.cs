﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Webwinkel.Models
{
    public class Category
    {
        // fields
        protected String name;
        protected String description;
        protected Int32 id;

        // Getters and setters
        [Display(Name = "Naam")]
        [ScaffoldColumn(true)]
        [Required]
        [MaxLength(255, ErrorMessage = "Naam bestaat uit maximum 255 karakters.")]
        public String Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        [Display(Name = "Beschrijving")]
        [ScaffoldColumn(true)]
        [MaxLength(1024, ErrorMessage = "Naam bestaat uit maximum 1024 karakters.")]
        public String Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        [Key]
        [ScaffoldColumn(false)]
        [Required]
        public Int32 Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
    }
}